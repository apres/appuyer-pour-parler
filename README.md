# But du tutoriel
Ce tutoriel va vous apprendre à activer et utiliser la fonction **Appuyer pour parler** dans Discord quand on l'utilise sur son ordinateur. Cette fonction vous fera non seulement gagner du temps mais elle permettra aussi que vous soyez plus réactives dans des le discussions.

# Par défaut
Sans modification Discord va detecter la voix et activer le micro dès qu'une entrée au niveau du micro est perçu. Ainsi il est necessaire de laisser votre micro tout le temps couper. Cela pour éviter que l'icone vous representant ne s'entoure de vert (ceci signalant que votre micro perçoit quelquechose. Allez sur un canal vocal, de préférence un ou vous ne serez pas dérangé et dites un mot à internet pour constater par vous même ce phénomène.

# Comment on active la fonction Appuyer pour Parler
Maintent on va activer cette fonction pour voir la différence. 
La première étape est de cliquer sur le petit symbole engrenage en bas à gauche de votre écran. 
Ensuite il va falloir cliquer sur *Voix & vidéos* au milieu à gauche, vous pouvez alors voir dans ce menu si votre micro fonctionne bien. 
Enfin cliquez *Appuyez pour Parler* en dessous de activation du micro pour le selectionnez. 
La dernière étape consiste à choisir la touche sur laquelle appuyer pour activer votre micro je vous conseille la touche *5* de votre pavé numérique ou la touche contrôle pour éviter les intéraction avec le canal texte sur lequel vous serez.

# Mais à quoi cela sert-il?
Revenez maintenant sur un canal pour tester cette fonction. Maintenant le cercle vert autour de votre icone apparaît uniquement quand vous appuyez sur la touche que vous avait choisi.
Plus besoin d'activer et de désactiver votre micro.
Cela à un autre avantage plus symbolique, cela montre à tous et toutes que vous êtes une actrices de la conversation. 
Même des personnes qui ne participent pas à la conversation verront que votre micro est tout le temps ouvert et qu'il vous suffit maintenant d'une touche à presser pour répondre du tac-o-tac.
